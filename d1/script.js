console.log("Hello World");

/*
	JSON

pang format ng data, when transporting or receiving it.

JavaScript Object Notation, is a data format used by application to store and transport data to one another.

Even Though it has the Javascript in its name, use of JSON is not Limited to JavaScript-based applications and can also be used in other programming languages.

Files that store JSON data are saved with a file extension of .json


bakit JSON?
	1. self-describing
	2. lightweight
	3. familiarity.
*/


//JSON Objects
	/*
		- JSON stands for JavaScript Object Notation.
		- JSON is also used in other programming languages.
		- JavaScript objects (muka syang object ng JS pero hindi talaga sya object.) not to be confused with JSON
		- Serialization is the process of converting data intoa series of bytes for easier transmission or transfer of information.
		- Uses double qoutes for property names.

		Syntax:
			{
				"propertyA": "valueA",
				"PropertyB": "valueB"
			}
	*/


//JSON Objects
	/*
	{
		"city": "Quezon City",
		"province": "Metro Manila",
		"country": "Philippines"
	}
	*/

//JSON Arrays
	/*
		"cities": [
			{ "city": "Quezon City", "province": "Metro Manila", "country": "Philippines" },
			{ "city": "Manila City", "province": "Metro Manila", "country": "Philippines" },
			{ "city": "Makati City", "province": "Metro Manila", "country": "Philippines" }
		]
	*/

//JSON Methods
	/*
		- The Json object contains methods for parsing and converting data into stringified JSON
	*/

//Converting Data into Stringified JSON
	/*
		- Stringigied JSON is a Javascript object converted into a string to be used in other functions of JavaScript application
	*/

	//kapag ang isang data ay pumunta sa code natin, hindi sya agad magagamit. para maging object sya at mabasa ng JS i coconvert sya into Object. At para maconvert sya, gagamit tayo ng parse, at stringify or ang tinatawag na Sirialization and desirialization.

	let batchesArr = [
		{batchName: "BatchX"},
		{batchName: "BatchY"}
	];

	/*
		- The "stringify" method is used to convert Javascript objects into a string.
	*/

	console.log(`Result from stingify method: `);
	console.log(JSON.stringify(batchesArr));



	let data = JSON.stringify({
		name: "John",
		age: 31,
		address: {
			city: "Manila",
			country: "Philippines"
		}
	})
	console.log(data);



//Using Stringify Method with Variables
	/*
		- When information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value  with a varibale.
		- This is commonly used when the information to be stored and sent to a backend application will come from a frontend application.

		Syntax:
			JSON.stringify ({
				propertyA: variableA,
				propertyB: varibaleB
			});
	*/

/*	let firstName = prompt(`What is your first name?`);
	let lastName = prompt(`What is your last name?`);
	let age = prompt(`What is your age?`);
	let address = {
		city: prompt(`Which city do you live in?`),
		country: prompt(`Which country does your city address belong to?`)
	};
	let otherData = JSON.stringify({
		firstName: firstName,
		lastName: lastName,
		age: age,
		address: address
	});
	console.log(otherData);*/



//Converting Stingified JSON into JavaScript Objects
	/*
		- Objects are common data types used in application because of the comlex data structures that can be created out of them.
		- Information is commonly sent to applications in stringified JSON and then converted back into objects
		- This happens both for sending information to a backend application and vice versa.
	*/

	let batchesJSON = `[
		{"batchName": "batchX"},
		{"batchName": "batchY"}
	]`;
	console.log(`Result form parse method`);
	console.log(JSON.parse(batchesJSON));

	let stringifiedObject = `{
		"name": "John",
		"age": "31",
		"address": {
			"city": "Manila",
			"country": "Philippines"
		}
	}`;
	console.log(JSON.parse(stringifiedObject));

	//Sirialization and Desirialization, ang tawag sa pag papasa paputa at pabalik ng mga data ng mga application.





//MongoDB - Database and NoSQL Introduction
	/*
		database ia an organized collection of information or data.
		
		Difference bet. information and data.
		- Data is raw and does not carry any specific meaning. information on the other hand is a group of organized data that contains logical meaning.
		- Information is a group of Data that portraits a meaning.


		Databases typically refer to information stored in a computer system. but it can also refer to physical databases.
		One such example is a library which is a database of books.

		A database is managed by what is called a "database management system".

		A database management system (DBMS) is a system specifically designed to manage the storage, retrieval and modification of data in a database.

		continuing on the example earlier, the DBMS of a physical library is the Dewey Decimal System.

		
		Database system allows 4 types of operations when it comes to handling data, namely create(insert), read(select), update and delete.
		collectively it is called the CRUD operation.

		Relational database is a type of database where data is stored as a set of tables with rows and columns (pretty much like a table printer on a piece of paper).

		for unstructured data (data that cannot fit into a strict tabular format), NoSQL database are commonly,...

		SQL = Structured Query Language. It is the language used typically in relational DBMS to store, retrieve, and modify.
	*/